package com.example.asolorzano.gps.servicios;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;

/**
 * Created by asolorzano on 16/07/16.
 */
public class MiServicio extends Service implements LocationListener {

    private final Context ctx;

    double latitud;
    double longitud;
    Location location;
    boolean gpsActivo;
    TextView texto;
    LocationManager locationManager;


    public MiServicio() {
        super();
        this.ctx = this.getApplicationContext();
    }

    public MiServicio(Context c) {
        super();
        this.ctx = c;
        getLocation();
    }

    public void setView(View v) {
        texto = (TextView) v;
        texto.setText("Coordenadas:" + latitud + "," + longitud);
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) this.ctx.getSystemService(LOCATION_SERVICE); //Obtiene las coordenadas
            gpsActivo = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
        }

        if (gpsActivo) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 1000 * 60, 10, this);

            location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER); //Última ubicación conocida
            latitud = location.getLatitude();
            longitud = location.getLongitude();


        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /***
     * Método que verifica si haz cambiando de ubicación GPS
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {

    }

    /***
     * Método que muestra el estatus de tu GPS
     * @param provider
     * @param status
     * @param extras
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    /**
     * Método que muestra si esta habilitado el GPS
     * @param provider
     */
    @Override
    public void onProviderEnabled(String provider) {

    }

    /***
     * Método que muestra si esta desabilitado el GPS
     * @param provider
     */
    @Override
    public void onProviderDisabled(String provider) {

    }
}
