package com.example.asolorzano.gps;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asolorzano.gps.servicios.MiServicio;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                .add(R.id.container,new PlaceholderFragment())
                .commit();

        //MiServicio servicio = new MiServicio(getApplicationContext());
    }


    public static class PlaceholderFragment extends Fragment{
        public PlaceholderFragment() {

        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            MiServicio servicio = new MiServicio(getActivity().getApplicationContext());
            servicio.setView(rootView.findViewById(R.id.TextoUbicacion));
            return rootView;

        }
    }
}
